class Timer {
	static MINUTE = 60 * 1000;
	static DECISEC = 100; // tenth of a second
	static subOffsets = [28, 36, 42, 50];
	static startTime;
	static intervalID;

	static draw() {
		let msecElapsed = Date.now() - Timer.startTime;
		let minuteSprites;
		let decisecSprites;
		let offsetX = Sprite.scoreboard.width * 2 + 8;
		let offsetY = 26;

		if (msecElapsed >= 10 * Timer.MINUTE) {
			cx2.drawImage(Sprite.sucks, offsetX, 0)
			return;
		}

		minuteSprites = base10ToSprites(Math.floor(msecElapsed / Timer.MINUTE), 1);
		decisecSprites = base10ToSprites(Math.floor(msecElapsed / Timer.DECISEC) % 600, 3);

		cx2.drawImage(Sprite.timer, offsetX, 0);

		minuteSprites.concat(decisecSprites).forEach(function(sprite, idx){
			cx2.drawImage(sprite, offsetX + Timer.subOffsets[idx], offsetY);
		});
	}

	static reset() {
		Timer.startTime = Date.now();
		Timer.intervalID = setInterval(Timer.draw, 50);
	}

	static stop() {
		clearInterval(Timer.intervalID);
	}
}
