var m;	// maze

let width = document.getElementById('w');
let height = document.getElementById('h');

let qs = new URLSearchParams(window.location.search);
function init() {
	m = Maze.random(Number(qs.get('w')) || 8, Number(qs.get('h')) || 6);
	history.replaceState(null,'',window.location.pathname + "?code=" + m.toBase64());
}
if (qs.get('code'))
	m = Maze.fromBase64(qs.get('code'));
else if (qs.get('share')) {
	let c = translateShareCode(qs.get('share'));
	if (c)
		m = Maze.fromBase64(c);
	else
		init();
}
else
	init();

width.value = m.width;
height.value = m.height;

var p = new Player(m);


function tick() {
	drawMaze(m);
	drawPlayer(p);
	drawState(p.state);
	drawScore(p.state, p.score);
}

async function share() {
	let link = 'https://loganhall.net/rainmaze/play.html';
	try {
		let r = await fetch(
			'share.php',
			{
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				body: new URLSearchParams({code: m.toBase64()}).toString()
			}
		);
		let s = await r.json();
		if (s) {
			link += '?share=' + s;
			let btn = document.getElementById('btn_share');
			btn.innerHTML = "\u2714";
			btn.style.border = '2px solid green';
			btn.style.color = 'green';

		}
		else {
			console.log('Could not generate sharecode');
			console.log(s);
			return;
		}
	}
	catch (e) {
		console.log(e);
		link += '?code=' + m.toBase64();
	}

	try {
		await navigator.clipboard.writeText(link);
	}
	catch (e) {
		alert('Copy: ' + link);
	}
}

function openNewMaze() {
	window.location = 'play.html?w=' + width.value + '&h=' + height.value;
}
function openEditor() {
	window.location = 'edit.html?' + new URLSearchParams(window.location.search).toString();
}

let begun = false;
function move(direction) {
	p.move(VEC[direction]);
	if (!begun) {
		begun = true;
		Timer.reset();
	}

	tick();
}
function reset() {
	let s = p.score;
	p = new Player(m);
	begun = false; // these four lines fix the janky reset, everything is reset as it ought to be
	Timer.reset();
	Timer.draw();
	Timer.stop();
	tick();
}
// TODO: consider moving all global events to a new file Events.js so they are all in one place
// also consider mapping the events onto the canvas rather than the document, and setting the canvas tabIndex so that it
// is "selectable" and giving it autofocus
document.addEventListener('keydown', e => {
	if (e.key.includes('Arrow')) {
		e.preventDefault();
		move(e.key.substring(5).toUpperCase());
	}
	else if (e.key == 'r')
		reset();
});
document.addEventListener('swiped', e => {
	e.preventDefault();
	move(e.detail.dir.toUpperCase());
});


window.addEventListener('resize', e => {
	tick();
});

window.addEventListener('load', e => {
	let btn_rules = document.querySelector('#btn_rules');
	let rules = document.querySelector('#rules');
	btn_rules.addEventListener('mouseenter', e => rules.style.display = 'block' );
	btn_rules.addEventListener('mouseleave', e => rules.style.display = 'none' );

	tick();
	Timer.draw();
	Timer.stop();
});
