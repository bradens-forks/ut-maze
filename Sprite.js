class Img extends Image {
	constructor(src) {
		super();
		this.src = src;
	}
}

class Sprite {
	static plaid = new Img('graphics/plaid.png');
	static heart = new Img('graphics/heart.png');
	static flavorless = new Img('graphics/flavor_none.png');
	static lemons = new Img('graphics/flavor_lemons.png');
	static oranges = new Img('graphics/flavor_oranges.png');
	static victory = new Img('graphics/flavor_victory.png');
	static scoreboard = new Img('graphics/score_ut.png');
	static sucks = new Img('graphics/timer_sucks.png');
	static dog = new Img('graphics/dog.png');
	static timer = new Img('graphics/timer_ut.png');
	static digit = [
		new Img('graphics/timer_0.png'),
		new Img('graphics/timer_1.png'),
		new Img('graphics/timer_2.png'),
		new Img('graphics/timer_3.png'),
		new Img('graphics/timer_4.png'),
		new Img('graphics/timer_5.png'),
		new Img('graphics/timer_6.png'),
		new Img('graphics/timer_7.png'),
		new Img('graphics/timer_8.png'),
		new Img('graphics/timer_9.png')
	];
}
