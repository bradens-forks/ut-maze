class Aud extends Audio {
	play() {
		this.currentTime = 0;
		super.play();
	}

	setVolume(volume) {
		this.volume = volume;
		return this;
	}
}

class Sound {
	static flavor = new Aud('sounds/snd_bell.wav').setVolume(0.8);
	static pirahnas = new Aud('sounds/snd_b.wav').setVolume(0.7);
	static green = new Aud('sounds/mus_mt_yeah.wav').setVolume(0.12);
	static shock = new Aud('sounds/snd_shock.wav').setVolume(0.5);
	static victory = new Aud('sounds/snd_dumbvictory.wav');
}
