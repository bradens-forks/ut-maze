const TILE_SIZE = 40;
var scale = 1;
let cx1;
let cx2;

const flavorSprites = [Sprite.flavorless, Sprite.oranges, Sprite.lemons, Sprite.victory];
/* array is ordered the same as the enum. This is a C idiom, but it
 * works fine here. Either way, it gets rid of the switch statement
 * -Braden
 */

function grayscale(x,y) {
	let z = Math.sin(x**3*y**4+x+y) * 10000;
	let rng = Math.floor((z - Math.floor(z)) * 7 + 8);
	return '#' + rng.toString(16).repeat(6);
}

function drawMaze(maze) {
	cx1.clearRect(0,0,canvas.width,canvas.height);

	let w = TILE_SIZE * maze.width;
	let h = TILE_SIZE * maze.height;
	let marginHeight = document.getElementsByTagName('header')[0].offsetHeight + document.getElementsByTagName('footer')[0].offsetHeight;
	// Magic numbers '16' and '48' are rough margins which are nigh-impossible to get with pure JS
    // TODO: use document.querySelector(tagname) instead of document.getElementsByTagName(tagname)[0]
	scale = Math.min((window.innerWidth - 16) / w, (window.innerHeight - marginHeight - 60) / h, 1);
	canvas.width = w*scale;
	canvas.height = h*scale;
	cx1.scale(scale,scale);


	for (let x=0; x<maze.width; x++) {
		for (let y=0; y<maze.height; y++) {
			let tile = maze[x][y];
			if (tile == TILE.PLAID)
				cx1.drawImage(Sprite.plaid,TILE_SIZE*x,TILE_SIZE*y,TILE_SIZE,TILE_SIZE);
			else {
				cx1.beginPath();
				cx1.rect(TILE_SIZE*x,TILE_SIZE*y,TILE_SIZE,TILE_SIZE);
				cx1.fillStyle = (COLOR[tile] || grayscale(x,y));
				cx1.fill();
				cx1.closePath();
			}
		}
	}
}

function drawPlayer(player) {
	let x = TILE_SIZE * player.x + TILE_SIZE / 8;
	let y = TILE_SIZE * player.y + TILE_SIZE / 8;
	let size = TILE_SIZE * 0.75;

	cx1.drawImage(Sprite.heart, x, y, size, size);
}

function base10ToSprites(num, minWidth) {
	return base10Explode(num, minWidth).map(digit => Sprite.digit[digit]);
}

function drawState(state) {
	cx2.drawImage(flavorSprites[state], 0, 0);
}

/* 1. TODO: decouple state/flavorboard code from score drawing code
 *    (find a better way to get the X offset)
 *    consider a separate canvas
 *
 * 2. The way this is done actually does change the behavior, as values
 *    are 0-padded rather than space-padded. But I think it looks better
 */
function drawScore(state, score) {
	let flavorboard = flavorSprites[state]; // 1

	if (score > 999) {
		cx2.drawImage(Sprite.dog, flavorboard.width + 31, 22);
		return;
	}

	cx2.drawImage(Sprite.scoreboard, flavorboard.width + 4, 0);

	base10ToSprites(score, 3).forEach(function(sprite, idx){ // 2
		let offsetX = flavorboard.width + 32 + idx * 6;

		cx2.drawImage(sprite, offsetX, 26);
	});
}

window.addEventListener('load', e => {
	cx1 = document.getElementById('canvas').getContext('2d');
	let sbar = document.getElementById('statusbar');
	if (sbar) {
		cx2 = sbar.getContext('2d');
		cx2.webkitImageSmoothingEnabled = false;
		cx2.mozImageSmoothingEnabled = false;
		cx2.imageSmoothingEnabled = false;
		cx2.scale(3,3);

		Timer.reset();
	}
});
